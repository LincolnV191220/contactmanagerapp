import { Component, OnInit } from '@angular/core';
import { IContact } from 'src/app/Models/iContacts';
import { ContactService } from 'src/app/Services/contact.service';

@Component({
  selector: 'app-contact-manager',
  templateUrl: './contact-manager.component.html',
  styleUrls: ['./contact-manager.component.css']
})
export class ContactManagerComponent implements OnInit {

  public loading: boolean = false;
  public contacts: IContact[] = []
  public errorMessage: string | null = null

  constructor(private ContactService: ContactService) { }

  ngOnInit(): void {
    this.getAllContactsFromServer();
  }

  public getAllContactsFromServer() {
    this.loading = true;
    this.ContactService.getAllContacts().subscribe((data) => {
      this.contacts = data;
      this.loading = false;
    }, error => {
      this.errorMessage = error;
      this.loading = false;
    });
  }

  public deleteContact(contactId: string | undefined) {
    if (contactId) {
      this.ContactService.deleteContact(contactId).subscribe(data => {
        this.getAllContactsFromServer();
      }, err => {
        this.errorMessage = err;
      });
    }
  }
}
