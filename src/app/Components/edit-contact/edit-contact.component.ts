import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IContact } from 'src/app/Models/iContacts';
import { IGroup } from 'src/app/Models/iGroup';
import { ContactService } from 'src/app/Services/contact.service';

@Component({
  selector: 'app-edit-contact',
  templateUrl: './edit-contact.component.html',
  styleUrls: ['./edit-contact.component.css']
})
export class EditContactComponent implements OnInit {

  public loading: boolean = false;
  public contactId: string | null = null;
  public contact: IContact = {} as IContact;
  public errorMessage: string | null = null;
  public groups: IGroup[] = [] as IGroup[];

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private contactService: ContactService) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(param => {
      this.contactId = param.get('contactId')
    });
    if (this.contactId) {
      this.loading = true
      this.contactService.getContact(this.contactId).subscribe(data => {
        this.contact = data;
        this.loading = false;
        this.contactService.getAllGroups().subscribe(data => {
          this.groups = data;
        });
      }, err => {
        this.errorMessage = err;
        this.loading = false;
      });
    }
  }

  public submitUpdate() {
    if (this.contactId) {
      this.contactService.updateContact(this.contact, this.contactId).subscribe((data) => {
        this.router.navigate(['/']).then();
      }, err => {
        this.errorMessage = err
        this.router.navigate([`/edit/${this.contactId}`]).then();
      })
    }
  }
}
